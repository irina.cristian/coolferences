import React from "react";

import "./style.css";

export function DisplayText({ children }: { children: React.ReactNode }) {
  return <h1 className="DisplayText">{children}</h1>;
}
