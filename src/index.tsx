import bugsnag from "bugsnag-js";
import createPlugin from "bugsnag-react";
import React from "react";
import { render } from "react-dom";

import { getConfiguration } from "./config";
import { App } from "./screens/App";

import "./style.css";

const {
  bugsnag: { apiKey }
} = getConfiguration();

const ErrorBoundary = bugsnag(apiKey).use(createPlugin(React));

render(
  <ErrorBoundary>
    <App />
  </ErrorBoundary>,
  document.querySelector(".js-app")
);
