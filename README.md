# coolferences

Web application used as playground / sandbox during ["Confident Software Delivery"](https://agiletestingdays.com/2018/session/confident-software-delivery/) workshop.

---

<!-- TOC depthFrom:2 -->

- [Requirements](#requirements)
  - [Local development](#local-development)
  - [CI/CD with GitLab](#cicd-with-gitlab)
  - [Deployment with surge.sh](#deployment-with-surgesh)
- [Dev workflow](#dev-workflow)
- [How-to](#how-to)
  - [Run tests](#run-tests)
  - [Run the application](#run-the-application)
  - [Access the deployed sites](#access-the-deployed-sites)
- [Git hooks](#git-hooks)

<!-- /TOC -->

---

## Requirements

### Local development

[Node.js](https://nodejs.org/en/) is the only global dependency required to run the project. Everything else is defined into the `package.json` file and will be handled by `npm` after executing `npm install`.

If you need to install Node the following links contain some instructions for different environments:

- [macOS](https://nodejs.org/en/download/package-manager/#macos)
- [Windows](https://nodejs.org/en/download/package-manager/#windows)
- [Debian and Ubuntu distributions](https://github.com/nodesource/distributions/blob/master/README.md#debinstall)

Additionally, a Node version management tool (such as [`nvm`](https://github.com/creationix/nvm) or [`n`](https://github.com/tj/n)) may be used.

**NOTE: this project is expected to work only with the Node version defined at [`.nvmrc`](./.nvmrc).**

### CI/CD with GitLab

This project makes use of GitLab CI to perform continuous integration and continuous delivery. It is possible to create an account for free by following [this link](https://gitlab.com/users/sign_in#register-pane). Once this is done, you may want to [fork](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html) the project to experiment or apply some changes into your own working copy.

### Deployment with surge.sh

[Surge](https://surge.sh/), a _simple, single-command web publishing_ tool is used to deploy the site on every commit into `master`, `production` and on new merge requests.

You can create a surge account by following the next steps (once Node is already installed on your system):

1. Type `npx surge token` on your terminal of choice. Remember to copy the token that will be displayed at the end of the process since it will be used in upcoming steps.

Pipelines for this project expect to have access to two environment variables to know who is deploying the site. Environment variables can be [easily created](https://docs.gitlab.com/ee/ci/variables/#variables) from the GitLab user interface for your forked repository. These are the variables expected by `surge` to automatically deploy your site:

- `SURGE_LOGIN`: your account email
- `SURGE_TOKEN`: the previous token displayed after executing `surge token`

## Dev workflow

This project makes use of two long-lived branches for the following purposes:

- `master`: work in progress. Every commit into this branch is expected to not break the build. `master` should be deployable at any time.
- `production`: this is the default branch and contains the code being already deployed in production. Every commit into `production` triggers a deployment. `master` is the only branch expected to be merged back into `production`.

Additionally, it is possible to create feature branches from `master` to develop independent user stories. Those branches have to be merged back into master when they get ready for release. Remember that every new merge request will create an environment on-demand which only includes the changes affected for that merge request. This will apply also for merge request between `master` and `production`.

## How-to

### Run tests

To run the tests you can use the following command: `npm run test`.

We're using [Jest](https://github.com/facebook/jest) as testing framework and test runner.

### Run the application

`npm run start` makes your app accessible at `localhost:1234` by default.

We're using [parcel](https://parceljs.org/), a zero-configuration bundle which happily takes care of all the plumbing involved in the building process :)

Use the `PORT` environment variable to customize the default port used by `parcel` on runtime:

```sh
PORT=3000 npm run start # makes the application to be available @ localhost:3000
```

### Access the deployed sites

By convention, every deployed site on surge.sh will be prefixed by a slug of the project's path and the deployed branch's name. For example, if the `master` branch for gitlab.com/johndoe/coolferences gets deployed it will be located at: <https://johndoe-coolferences-master.surge.sh/>.

There is only one exception for the production deployment which only uses the project's path. So deploying the production branch for gitlab.com/johndoe/coolferences will make the site available at: <https://johndoe-coolferences.surge.sh/>.

On-demand sites could be directly accessed through the GitLab UI for Merge Requests as well as from the sidebar: `Operations > Environments`.

## Git hooks

We're using [husky](https://github.com/typicode/husky) to prevent bad pushes to remote.

_Husky_ uses [Git Hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks) under the hood to ensure a successful test suite run and lack of linter or compiler errors.

---

`coolferences` is licensed under the [GNU General Public License v3.0](https://choosealicense.com/licenses/gpl-3.0/).
