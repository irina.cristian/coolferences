import React from "react";

import { shallow } from "enzyme";

import { DisplayText } from "../../../src/components/DisplayText";

it("uses a h1 tag to wrap the text", () => {
  const subject = shallow(<DisplayText>Hello, world!</DisplayText>);

  expect(subject.find("h1")).toHaveLength(1);
  expect(subject.find("h1").text()).toEqual("Hello, world!");
});

it("uses the right class name", () => {
  const subject = shallow(<DisplayText>Hello, world!</DisplayText>);

  expect(subject.find("h1").hasClass("DisplayText")).toBe(true);
});
